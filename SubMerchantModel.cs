﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixPayService
{
    public class SubMerchantModel
    {
        public string SubMerchantID { get; set; }
        public string PrimaryContactID { get; set; }
    }
}
