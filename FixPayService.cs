﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace FixPayService
{
    public class FixPayService
    {
        public void Start()
        {
            try
            {
                var connect = new SqlConnection(ConfigurationManager.ConnectionStrings["UKTDConn"].ConnectionString);
                connect.Open();

                Console.WriteLine(DateTime.Now.ToString("MM-dd-yyyy"));


                if (!Directory.Exists(@"D:\UKTDFixPayService"))
                {
                    Directory.CreateDirectory(@"D:\UKTDFixPayService");
                }
                var subMerchantList = connect.Query<SubMerchantModel>("SELECT [SubMerchantID],[PrimaryContactID] FROM [UKTD].[dbo].[SubmerchantDetails]", default);

                foreach (var subMech in subMerchantList)
                {
                    string subMerchantId = subMech.SubMerchantID;
                    string primaryContactId = subMech.PrimaryContactID;
                    var paraSearch = new DynamicParameters();
                    paraSearch.Add("@PrimaryContactID", primaryContactId);
                    paraSearch.Add("@subMerchantID", subMerchantId);
                    var result = connect.Query<Transaction>("[dbo].[UKTD_FixedFeeDetail]", paraSearch, commandType: CommandType.StoredProcedure);
                }

            }
            catch (Exception ex)
            {
                string[] s = { ex.Message };
                File.AppendAllLines(@"D:\UKTDFixPayService\Log.txt", s);
            }
        }

        public void Stop()
        {

        }
    }
}
