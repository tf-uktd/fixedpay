﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace FixPayService
{
    internal static class ConfigureService
    {
        internal static void Configure()
        {
            HostFactory.Run(configure =>
            {
                configure.Service<FixPayService>(service =>
                {
                    service.ConstructUsing(s => new FixPayService());
                    service.WhenStarted(s => s.Start());
                    service.WhenStopped(s => s.Stop());
                });

                configure.RunAsLocalSystem();
                configure.SetServiceName("FixPayService");
                configure.SetDisplayName("FixPayService");
                configure.SetDescription("This service is to calculate the Deposite limit");
            });
        }
    }
}
